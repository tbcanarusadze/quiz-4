package com.example.quiz4

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val itemsList = mutableListOf<ItemModel>()
    private val rowList = mutableListOf<ItemModel>()
    private val columnList = mutableListOf<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter
    private var isFirstPlayer = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(itemsList, object : RecyclerViewAdapter.ItemOnClick {
            override fun onClick(position: Int, itemView: View) {
                isFirstPlayer = if (isFirstPlayer) {
                    itemView.setBackgroundResource(R.mipmap.x)
                    false
                } else {
                    itemView.setBackgroundResource(R.mipmap.o)
                    true
                }
                itemView.isClickable = false

            }

            override fun setWinner(position: Int, model: ItemModel, itemView: View) {


            }


        })
        enterButton.setOnClickListener {
            val spanCount = numberEditText.text.toString().toInt()
            var rowCount = 0
            var columnCount = 0
            itemsList.clear()
            recyclerView.layoutManager = GridLayoutManager(this, spanCount)
            recyclerView.adapter = adapter
            for (item in 1..spanCount * spanCount) {
                itemsList.add(
                    0,
                    ItemModel(item.toString(), rowCount, columnCount)
                )
                columnCount++
                if (columnCount == spanCount) {
                    columnCount = 0
                    rowCount += 1

                }

            }
            adapter.notifyDataSetChanged()

        }


    }


}
