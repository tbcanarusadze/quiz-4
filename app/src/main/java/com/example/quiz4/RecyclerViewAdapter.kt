package com.example.quiz4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<ItemModel>, private val itemOnClick: ItemOnClick) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),  View.OnClickListener {
        fun onBind() {
            itemView.setOnClickListener(this)


        }
        override fun onClick(v: View?) {
            itemOnClick.onClick(adapterPosition, itemView)

        }

    }

    interface ItemOnClick {
        fun onClick(position:Int, itemView: View)
        fun setWinner(position: Int, model: ItemModel, itemView: View)
    }

}